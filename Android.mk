LOCAL_PATH := $(call my-dir)

ifneq ($(QCPATH),)
include $(LOCAL_PATH)/build/target_specific_features.mk
include $(call all-makefiles-under,$(LOCAL_PATH))
else
include $(LOCAL_PATH)/gnsspps/Android.mk
endif
